import Vue from 'vue'
import App from './App.vue'
import router from './router'
import VueToastr2 from 'vue-toastr-2'
import 'vue-toastr-2/dist/vue-toastr-2.min.css'
import Vuetable from 'vuetable-2/src/components/Vuetable'



Vue.config.productionTip = false;
window.toastr = require('toastr');

Vue.use(VueToastr2);
Vue.use(Vuetable);

new Vue({
  router,
  render: h => h(App)
}).$mount('#app');
